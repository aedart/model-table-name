<?php  namespace Aedart\Model\Table\Name\Exceptions; 

/**
 * Class Invalid Table Name Exception
 *
 * Throw this exception when an invalid table name has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Table\Name\Exceptions
 */
class InvalidTableNameException extends \InvalidArgumentException{

}