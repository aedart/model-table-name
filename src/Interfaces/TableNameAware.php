<?php  namespace Aedart\Model\Table\Name\Interfaces;
use Aedart\Model\Table\Name\Exceptions\InvalidTableNameException;

/**
 * Interface Table Name Aware
 *
 * Components that implement this, promise that a table name can be specified and retrieve,
 * when it is needed. In addition, a default table name might be available, if none has
 * been set prior to obtaining it (implementation dependent).
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Table\Name\Interfaces
 */
interface TableNameAware {

    /**
     * Set the table name
     *
     * @param string $name A table name
     *
     * @return void
     *
     * @throws InvalidTableNameException If given table name is invalid
     */
    public function setTableName($name);

    /**
     * Get the table name
     *
     * If no table name has been set, this method sets
     * and returns a default table name, if such is
     * available
     *
     * @see getDefaultTableName()
     *
     * @return string|null The table name or null if none has been set
     */
    public function getTableName();

    /**
     * Get a default table name, if any is available
     *
     * @return string|null A default table name or null if no default is available
     */
    public function getDefaultTableName();

    /**
     * Check if a table name has been set
     *
     * @return bool True if a table name has been set, false if not
     */
    public function hasTableName();

    /**
     * Check if a default table name is available
     *
     * @return bool true if a default table name is available, false if not
     */
    public function hasDefaultTableName();

    /**
     * Check if the given table name is valid; e.g. if it follows a specific syntax,
     * has or has not specific chars, etc.
     *
     * @param mixed $name The table name to be validated
     *
     * @return bool true if the given table name is valid, false if not
     */
    public function isTableNameValid($name);
}