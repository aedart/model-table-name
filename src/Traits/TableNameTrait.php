<?php  namespace Aedart\Model\Table\Name\Traits; 

use Aedart\Model\Table\Name\Exceptions\InvalidTableNameException;
use Aedart\Validate\String\NonEmptyStringValidator;

/**
 * Trait Table Name
 *
 * @see \Aedart\Model\Table\Name\Interfaces\TableNameAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Table\Name\Traits
 */
trait TableNameTrait {

    /**
     * The table name
     *
     * @var string|null
     */
    protected $tableName = null;

    /**
     * Set the table name
     *
     * @param string $name A table name
     *
     * @return void
     *
     * @throws InvalidTableNameException If given table name is invalid
     */
    public function setTableName($name){
        if(!$this->isTableNameValid($name)){
            throw new InvalidTableNameException(sprintf('%s is not a valid table name', var_export($name)));
        }
        $this->tableName = $name;
    }

    /**
     * Get the table name
     *
     * If no table name has been set, this method sets
     * and returns a default table name, if such is
     * available
     *
     * @see getDefaultTableName()
     *
     * @return string|null The table name or null if none has been set
     */
    public function getTableName(){
        if(!$this->hasTableName() && $this->hasDefaultTableName()){
            $this->setTableName($this->getDefaultTableName());
        }
        return $this->tableName;
    }

    /**
     * Get a default table name, if any is available
     *
     * @return string|null A default table name or null if no default is available
     */
    public function getDefaultTableName(){
        return null;
    }

    /**
     * Check if a table name has been set
     *
     * @return bool True if a table name has been set, false if not
     */
    public function hasTableName(){
        if(!is_null($this->tableName)){
            return true;
        }
        return false;
    }

    /**
     * Check if a default table name is available
     *
     * @return bool true if a default table name is available, false if not
     */
    public function hasDefaultTableName(){
        if(!is_null($this->getDefaultTableName())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given table name is valid; e.g. if it follows a specific syntax,
     * has or has not specific chars, etc.
     *
     * @param mixed $name The table name to be validated
     *
     * @return bool true if the given table name is valid, false if not
     */
    public function isTableNameValid($name){
        // It is very tempting to attempt creating some kind of regex for
        // valid table names, e.g. for SQL-based systems or something like
        // that. However, sadly I was not able to find any good documentation
        // on "common/general" valid table names for MySQL, SQLite, Oracle,...etc
        // Therefore, the safest option - provided this actually is used
        // in some kind of database model, is to leave the actual validation
        // to the database-layer. Thus, here we just wish for a none-empty
        // string.
        //
        // If your model does, however, require a different kind of validation,
        // just override this method.
        return NonEmptyStringValidator::isValid($name);
    }

}