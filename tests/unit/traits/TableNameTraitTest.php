<?php

use Aedart\Model\Table\Name\Interfaces\TableNameAware;
use Aedart\Model\Table\Name\Traits\TableNameTrait;
use Faker\Factory as FakerFactory;

/**
 * Class TableNameTraitTest
 *
 * @coversDefaultClass Aedart\Model\Table\Name\Traits\TableNameTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class TableNameTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Model\Table\Name\Interfaces\TableNameAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Model\Table\Name\Traits\TableNameTrait');
        return $m;
    }

    /**
     * Get a dummy class
     *
     * @return TableNameAware
     */
    protected function getDummyClass(){
        return new DummyTableNameClass();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultTableName
     * @covers ::getDefaultTableName
     */
    public function getNullAsDefaultTableName(){
        $trait = $this->getTraitMock();
        $this->assertFalse($trait->hasDefaultTableName());
        $this->assertNull($trait->getDefaultTableName());
    }

    /**
     * @test
     * @covers ::getTableName
     * @covers ::hasTableName
     * @covers ::hasDefaultTableName
     * @covers ::setTableName
     * @covers ::getDefaultTableName
     * @covers ::isTableNameValid
     */
    public function getDefaultTableName(){
        $dummy = $this->getDummyClass();
        $this->assertNotNull($dummy->getTableName());
    }

    /**
     * @test
     * @covers ::getTableName
     * @covers ::hasTableName
     * @covers ::setTableName
     * @covers ::isTableNameValid
     */
    public function setAndGetTableName(){
        $trait = $this->getTraitMock();

        $name = $this->faker->word;

        $trait->setTableName($name);

        $this->assertSame($name, $trait->getTableName());
    }

    /**
     * @test
     * @covers ::setTableName
     * @covers ::isTableNameValid
     *
     * @expectedException \Aedart\Model\Table\Name\Exceptions\InvalidTableNameException
     */
    public function attemptSetInvalidTableName(){
        $trait = $this->getTraitMock();

        $name = '';

        $trait->setTableName($name);
    }
}

class DummyTableNameClass implements TableNameAware {
    use TableNameTrait;

    public function getDefaultTableName() {
        $faker = FakerFactory::create();
        return $faker->word;
    }
}