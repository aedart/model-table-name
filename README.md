## Model-Table-Name ##

Getter and Setter package for a model table name

This package is part of the Aedart\Model namespace; visit https://bitbucket.org/aedart/model to learn more about the project.

Official sub-package website (https://bitbucket.org/aedart/model-table-name)

## Contents ##

[TOC]

## When to use this ##

When your component(s) need to be aware of an email

## How to install ##

```
#!console

composer require aedart/model-table-name 1.*
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Quick start ##

Provided that you have an interface, e.g. for a repository, you can extend the table-name-aware interface;

```
#!php
<?php
use Aedart\Model\Table\Name\Interfaces\TableNameAware;

interface IRepository extends TableNameAware {

    // ... Remaining interface implementation not shown ...
    
}

```

In your concrete implementation, you simple use the table-name-traits;
 
```
#!php
<?php
use Aedart\Model\Table\Name\Traits\TableNameTrait;

class MyRepository implements IRepository {
 
    use TableNameTrait;

    // ... Remaining implementation not shown ... 
 
}
```

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package